#include<stdio.h>
#include<string.h>
#include<time.h>
#include<stdlib.h.>


typedef struct date
{
    int day;
    int month;
    int year;
}date_t;
typedef struct emp
{
    int id;
    char name[50];
    char add[100];
    float salary;
    date_t birth_date;
    date_t join_date;
}emply_t;
void accept_date(date_t *d);
void display_data(date_t *d);
int is_leap(int year);
void accept_employee(emply_t *e);
int age_when_joined(int b,int j);
date_t date_current();
int exp_months(date_t now, date_t join);
date_t date_add(date_t d, int days);
void accept_date(date_t *d)
{
    printf("date: ");
    scanf("%d%d%d",&d->day,&d->month,&d->year);
}

void display_data(date_t *d)
{
    printf("%d/%d/%d\n",d->day,d->month,d->year);
}
void accept_employee(emply_t *e)
{
    printf("Enter the employee id: ");
    scanf("%d",&e->id);
    printf("Enter the employee name: ");
    scanf("%s",e->name);
    printf("Enter the employee address: ");
    scanf("%s",e->add);
    printf("Enter the employee salary: ");
    scanf("%f",&e->salary);
    printf("Enter the employee birth ");
    accept_date(&e->birth_date);
    printf("Enter the employee joining ");
    accept_date(&e->join_date);
}
int main(void)
{
    int age,exp;
    date_t now,end_prob;
    emply_t info;
    accept_employee(&info);
    age=age_when_joined(info.birth_date.year,info.join_date.year);
    printf("Age when employee joined:%d\n",age);
    now=date_current();
    exp=exp_months(now,info.join_date);
    printf("Total experience in months:%d\n",exp);
    end_prob=date_add(info.join_date,90);
    printf("End of probation ");
    display_data(&end_prob);
    return 0;
}
int age_when_joined(int b,int j)
{
    int age;
    age=j-b;
    return age;
}
date_t date_current() 
{
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	date_t now;
	now.day = tm->tm_mday;
	now.month = tm->tm_mon + 1;
	now.year = tm->tm_year + 1900;
	return now;
}

int exp_months(date_t now, date_t join)
{
    int yrs,months,total;
    yrs=(now.year)-(join.year);
    months=(now.month)-(join.month);
    total=12*yrs+months;
    return total;
}

int is_leap(int year)
{
    if((year%4==0&&year%100!=0)||year%400==0)
    {
        return 1;
    }
    return 0;
}

date_t date_add(date_t d, int days)
{
    while(days>0)
    {
        days--;
        d.day=d.day+1;
        switch(d.month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            if(d.day>31)
            {
                d.month++;
                d.day=d.day-31;
            }
            break;

            case 4:
            case 6:
            case 9:
            case 11:
            if(d.day>30)
            {
                d.month++;
                d.day=d.day-30;
            }
            
            break;

            case 12:
            if(d.day>31)
            {
                d.month=1;
                d.year++;
                d.day=d.day-31;
            }
            
            break;

            case 2:
            if(is_leap(d.year))
            {
                if(d.day>29)
                {
                    d.month++;
                    d.day=d.day-29;
                }
                
            }
            else 
            {
                if(d.day>28)
                {
                    d.month++;
                    d.day=d.day-28;
                }

            }
            break;


        }

    }
    return d;
}
