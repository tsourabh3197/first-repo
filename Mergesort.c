/*
 ============================================================================
 Name        : Merge_final.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
#include<stdio.h>
#include<stdlib.h>
typedef struct book {
	int id;
	char name[40];
	int price;
}book_t;

void merge_sort(book_t arr[], int left, int right) {
	int mid, i, j, k, n;
	book_t *temp;
	if(left == right || left > right)
		return;
	mid = (left + right) / 2;
	merge_sort(arr, left, mid);
	merge_sort(arr, mid+1, right);
	n = right - left + 1;
	temp = (book_t*) malloc(n * sizeof(book_t));
	i = left;
	j = mid+1;
	k = 0;
	while(i <= mid && j <= right) {
		if(arr[i].price > arr[j].price) {
			temp[k] = arr[i];
			i++;
			k++;
		}
		else {
			temp[k] = arr[j];
			j++;
			k++;
		}
	}
	while(i <= mid) {
		temp[k] = arr[i];
		i++;
		k++;
	}
	while(j <= right) {
		temp[k] = arr[j];
		j++;
		k++;
	}
	for(i=0; i<n; i++)
		arr[left + i] = temp[i];
	free(temp);
}

int main() {
	book_t arr[10] = {
		{7, "Atlas Shrugged", 734},
		{1, "The Alchemist", 623},
		{5, "The Fountainhead", 532},
		{3, "Wings of Fire", 325},
		{4, "Yugandhar", 587},
		{8, "Mrityunjay", 973},
		{9, "Rich dad & Poor dad", 534},
		{10, "Monk who sold his ferrari", 238},
		{6, "Chhava", 592},
		{2, "The secret", 351}
	};
	int i, len = 10;
	merge_sort(arr, 0, len-1);
	for(i=0; i<len; i++)
		printf("%d, %s, %d\n", arr[i].id, arr[i].name, arr[i].price);
	printf("\n");
	return 0;
}
