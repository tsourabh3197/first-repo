#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MOVIE_DB	"movies.csv"
typedef struct movie {
	int id;
	char name[80];
	char genres[220];
}movie_t;

typedef struct node{
    movie_t m;
    struct node *next;
}node_t;
void copy_movie(movie_t *m,node_t *nnode);
node_t * create_new_node(movie_t *m);
void dispay_all_node();
void movie_display(movie_t *m);
void find_movie_by_name();
void find_movie_by_genre();
node_t *head= NULL;
void add_to_list(movie_t *m)
{
    node_t *nnode= create_new_node(m);
    if(head==NULL)
    {
        head=nnode;
    }
    else
    {
        node_t *trav=head;
        while(trav->next!=NULL)
        {
            trav=trav->next;
        }
        trav->next=nnode;

    }
    nnode=NULL;
    printf("Added to the linked list successfully..\n");
}
node_t * create_new_node(movie_t *m)
{
    node_t *nnode= (node_t *)malloc(sizeof(node_t));
    copy_movie(m,nnode);
    nnode->next=NULL;
    return nnode;

}
void copy_movie(movie_t *m,node_t *nnode)
{
    nnode->m.id =m->id;
    strcpy(nnode->m.name,m->name);
    strcpy(nnode->m.genres,m->genres);
}
void dispay_all_node()
{
    node_t *trav=head;
        if(trav==NULL)
        {
            printf("No nodes in list..\n");
        }
        while(trav->next!=NULL)
        {
            movie_display(&trav->m);
            trav=trav->next;
            
        }
        

}
void movie_display(movie_t *m) {
	printf("id=%d, name=%s, genres=%s\n", m->id, m->name, m->genres);
}

int parse_movie(char line[], movie_t *m) {
	int success = 0;
	char *id, *name, *genres;
	id = strtok(line, ",\n");
	name = strtok(NULL, ",\n");
	genres = strtok(NULL, ",\n");
	if(id == NULL || name == NULL || genres == NULL)
		success = 0; // partial record
	else {
		success = 1;
		m->id = atoi(id);
		strcpy(m->name, name);
		strcpy(m->genres, genres);
	}
	return success;
}

void load_movies() {
	FILE *fp;
	movie_t m;
    char line[1024];
	fp = fopen(MOVIE_DB, "r");
	if(fp == NULL) {
		perror("failed to open movies file");
		exit(1);
	}

	while(fgets(line, sizeof(line), fp) != NULL) {
		parse_movie(line,&m);
        movie_display(&m);
        add_to_list(&m);
	}
    
	fclose(fp);

}
void find_movie_by_name() {
	char name[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie name to be searched: ");
	gets(name);

	trav = head;
	while(trav != NULL) {
       // printf("inw\n");
		if(strstr(name, trav->m.name) != NULL) {
			movie_display(&trav->m);
			found = 1;
			break;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}

void find_movie_by_genre() {
	char genre[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie genre to be searched: ");
	gets(genre);

	trav = head;
	while(trav != NULL) {
		if(strstr(trav->m.genres, genre) != NULL) {
			movie_display(&trav->m);
			found = 1;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}
int main() {
	
	load_movies();
    dispay_all_node();
    find_movie_by_name();
    find_movie_by_genre();
	return 0;
}