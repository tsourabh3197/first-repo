/*
 ============================================================================
 Name        : Question_9.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"inventory.h"
int menu_list();
int main(void) {
	setvbuf(stdout,NULL,_IONBF,0);
	int choice;
	while(1)
	{
		choice=menu_list();
		switch(choice)
		{
		case 0:
			exit(0);
		case 1:
			add_record();
			break;
		case 2:
			display_record();
			break;
		case 3:
			find_record();
			break;
		case 4:
			edit_record();
			break;
		case 5:
			delete_record();
			break;
		}
	}
	return EXIT_SUCCESS;
}
int menu_list()
{
	int choice;
	printf("0. Exit\n");
	printf("1. Add record\n");
	printf("2. display record\n");
	printf("3. find record\n");
	printf("4. edit record\n");
	printf("5. delete record\n");
	printf("Enter your choice ");
	scanf("%d",&choice);
	return choice;
}
