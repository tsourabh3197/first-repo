/*
 * inventory.c
 *
 *  Created on: 15-Oct-2020
 *      Author: Sourabh
 */
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"inventory.h"
void add_record()
{
	inventory_t in;
	inventory_accept(&in);
	FILE *fp=fopen(INVENTORY_DB,"ab");
	if(fp==NULL)
	{
		perror("File can't be opened..\n");
		return;
	}
	fwrite(&in,sizeof(inventory_t),1,fp);
	inventory_display(&in);
	fclose(fp);

}
void inventory_accept(inventory_t *in)
{
	in->id= get_next_inve_id();
	printf("Name: ");
	scanf("%s",in->name);
	printf("price: ");
	scanf("%f",&in->price);
	printf("Quantity: ");
	scanf("%d",&in->quantity);
}
int get_next_inve_id()
{
	int max=0;
	inventory_t in;
	long size= sizeof(inventory_t);
	FILE *fp=fopen(INVENTORY_DB,"rb");
	if(fp==NULL)
		{
			return max+1;
		}
	fseek(fp,-size,SEEK_END);
	if(fread(&in,size,1,fp)>0)
		max= in.id;
	return max+1;
}
void inventory_display(inventory_t *in)
{
printf("ID: %d Name: %s Price: %.2f Quant.: %d\n",in->id,in->name,in->price,in->quantity);
}
void display_record()
{
	inventory_t in;
	long size= sizeof(inventory_t);
	FILE *fp=fopen(INVENTORY_DB,"rb");
	if(fp==NULL)
	{
		perror("file can't be opened..\n");
		return;
	}
	while(fread(&in,size,1,fp)>0)
	{
		inventory_display(&in);
	}
	fclose(fp);
}

void find_record()
{
	inventory_t in;
	char name[50];
	int found=0;
	printf("Enter name to be found..");
	scanf("%s",name);
	long size= sizeof(inventory_t);
	FILE *fp=fopen(INVENTORY_DB,"rb");
	if(fp==NULL)
	{
		perror("file can't be opened..\n");
		return;
	}
	while(fread(&in,size,1,fp)>0)
	{
		if(strstr(in.name,name)!=NULL)
		{
			inventory_display(&in);
			found=1;
		}
	}
	if(!found)
		printf("No such item is found..\n");
	fclose(fp);
}
void edit_record()
{
	inventory_t in;
		int id;
		int found=0;
		long size= sizeof(inventory_t);
		FILE *fp1=fopen(INVENTORY_DB,"rb");
		if(fp1==NULL)
		{
			perror("file can't be opened..\n");
			return;
		}
		while(fread(&in,size,1,fp1)>0)
		{
			inventory_display(&in);
		}

		fclose(fp1);
		printf("Enter id of item to be edited: ");
		scanf("%d",&id);
		FILE *fp=fopen(INVENTORY_DB,"rb+");
		if(fp==NULL)
		{
			perror("file can't be opened..\n");
			return;
		}
		while(fread(&in,size,1,fp)>0)
		{
			if(in.id==id)
			{
				found=1;
				break;
			}
		}
		if(found)
		{
			fseek(fp,-size,SEEK_CUR);
			inventory_accept(&in);
			fwrite(&in,size,1,fp);
			printf("Record is updated successfully...\n");
		}
		else
			printf("No such record is found..\n");

		fclose(fp);
}
void delete_record()
{
	inventory_t in;
			int id;
			int found=0;
			long size= sizeof(inventory_t);
			FILE *fp1=fopen(INVENTORY_DB,"rb");
			if(fp1==NULL)
			{
				perror("file can't be opened..\n");
				return;
			}
			while(fread(&in,size,1,fp1)>0)
			{
				inventory_display(&in);
			}

			fclose(fp1);
			printf("Enter id of item to be deleted: ");
			scanf("%d",&id);
			FILE *fp=fopen(INVENTORY_DB,"rb+");
			if(fp==NULL)
			{
				perror("file can't be opened..\n");
				return;
			}
			while(fread(&in,size,1,fp)>0)
			{
				if(in.id==id)
				{
					found=1;
					break;
				}
			}
			if(found)
			{
				fseek(fp,-size,SEEK_CUR);
				if(fread(&in,size,1,fp)>0)
				{
					in.id=0;
					strcpy(in.name,"");
					in.price=0;
					in.quantity=0;
					fseek(fp,-size,SEEK_CUR);
					fwrite(&in,size,1,fp);
					printf("Item is deleted successfully..\n");
				}
			}
			else
				printf("No such record is found..\n");

			fclose(fp);
}
