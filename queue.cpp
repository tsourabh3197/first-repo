//============================================================================
// Name        : Question_6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string>
using namespace std;
#define SIZE 3
enum menu_options{ EXIT, ENQUEUE, DEQUEUE,DISPLAY };
int menu(void);
class student
{
	int rollno;
	string name;
	int std;
	string sub[2];
	int marks[2];
public:
	student()
	{
		this->rollno=0;
		this->name="";
		this->std=0;
		for(int i=0;i<2;i++)
		{
			this->sub[i]="";
			this->marks[i]=0;
		}
	}


	void acceptRecord()
	{
		cout<<"Roll no: ";
		cin>>this->rollno;
		cout<<"Name:";
		cin>>name;
		cout<<"Std: ";
		cin>>this->std;
		for(int i=0;i<2;i++)
		{
			cout<<"sub: ";
			cin>>this->sub[i];
			cout<<"mark: ";
			cin>>this->marks[i];
		}
	}
	void printRecord()
	{
		cout<<"Roll no : "<<this->rollno<<endl;
		cout<<"Name    : "<<this->name<<endl;
		cout<<"std     : "<<this->std<<endl;
		for(int i=0;i<2;i++)
		{

			cout<<"sub  : "<<this->sub[i]<<endl;
			cout<<"Mark : "<<this->marks[i]<<endl;
		}
	}
};
class cqueue
{
	student s[SIZE];
	int rear;
	int front;
public:
	cqueue()
	{
		this->front=-1;
		this->rear=-1;
	}
	int is_queue_full();
	void acceptqueue();
	void displayqueue();
	int is_queue_empty();
	void dequeue();

};
int ct=0;
int main() {
	cqueue q;

	 //int choice;
while(1)
	{
		int choice=menu();
		switch(choice)
		{
		case EXIT:
			exit(0);
		case ENQUEUE:
			if(!q.is_queue_full())
			{
				q.acceptqueue();
				cout<<"Enqueued Successfully...."<<endl;
			}
			else
				cout<<"Queue is full"<<endl;
			break;
		case DEQUEUE:
			if(!q.is_queue_empty())
			{
				q.dequeue();
				cout<<"dequeued successfully...."<<endl;
			}
			else
				cout<<"Queue is empty..."<<endl;

			break;
		case DISPLAY:
			if(!q.is_queue_empty())
			{
				q.dequeue();
				cout<<"dequeued successfully...."<<endl;
			}
			else
			{
				q.displayqueue();
			}
			break;
		default:
			printf("Enter correct choice..\n");
			break;
		}
	}
return 0;
}
int menu(void)
{
	int choice;
	cout<<"Enter your choice"<<endl;
	cout<<"0.EXIT"<<endl;
	cout<<"1.ENQUEUE"<<endl;
	cout<<"2.DEQUEUE"<<endl;
	cin>>choice;
	return choice;
}
int cqueue::is_queue_full()
{
	return(ct==SIZE);
}
void cqueue::acceptqueue()
{
	if(this->front==-1)
		{
			this->front=0;
		}
		this->rear=(this->rear+1)%SIZE;
		this->s[this->rear].acceptRecord();
		ct++;
}
int cqueue:: is_queue_empty()
{
	return(ct==0);
}
void cqueue::dequeue()
{
	this->front=(this->front+1)%SIZE;
	ct--;
}
void cqueue::displayqueue()
{
	cout<<"front: "<<this->front<<endl;
	cout<<"rear: "<<this->rear<<endl;
	this->s[this->rear].printRecord();

}
