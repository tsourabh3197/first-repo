/*
 ============================================================================
 Name        : Question_3.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
int menu_list(void);
int is_array_full(int *aindex);
int is_array_empty(int *nindex);
void Select_Sort(int *pa);
void display(int*arr,int *aindex,int *nindex);
void add_number_in_array(int number,int*,int*,int *);
void delete_number_from_array(int *aindex,int *arr,int*nindex);
int sum_of_numbers_in_array(int *arr);
int maximum_number_in_array(int *arr);
int minimum_number_in_array(int *arr);

int main(void) {setvbuf(stdout,NULL,_IONBF,0);
int arr[10],aindex[10],nindex[10];
int choice,i,max,min,sum;
for(i=0;i<10;i++)
{
	arr[i]=0;
	aindex[i]=i;
	nindex[i]=-1;
}
	while(1)
	{
		switch(choice=menu_list())
		{
		case 0: exit(0);
		case 1:
			if(is_array_full(aindex))
			{
				printf("Array is full...!\n");
			}
			else
			{
				int number;
				printf("Enter Number to be added : ");
				scanf("%d",&number);
				add_number_in_array(number,aindex,arr,nindex);
			}
			break;
		case 2:
			if(is_array_empty(nindex))
			{
				printf("Array is empty!!!\n");
			}
			else
			{
				delete_number_from_array(aindex,arr,nindex);
			}
			break;
		case 3:
			max= maximum_number_in_array(arr);
			printf("Maximum number of array arr is %d\n",max);
			break;
		case 4:
			 min=minimum_number_in_array(arr);
			printf("Minimum number of array arr is %d\n",min);
			break;
		case 5:
			 sum=sum_of_numbers_in_array(arr);
			printf("Sum of numbers of array is %d\n",sum);
			break;

		case 6:
			display(arr,aindex,nindex);
			break;
		}
	}


	return EXIT_SUCCESS;
}
int menu_list(void)
{
	int choice;
	printf("0. Exit\n");
	printf("1. Add Number\n");
	printf("2. Delete Number\n");
	printf("3. Find Maximum Number\n");
	printf("4. Find Minimum Number\n");
	printf("5. Sum\n");
	printf("6. Display\n");
	scanf("%d",&choice);
	return choice;

}
int is_array_full(int* aindex)
{
	int i,j=0;
	for(i=0;i<10;i++)
		{
			if(aindex[i]==-1)
				j++;
		}
	if(j==10)
		return 1;
	else
		return 0;

}
void add_number_in_array(int number,int *aindex,int *arr,int *nindex)
{
	int pos;
	printf("Available Positions are: ");
	for(int i=0;i<10;i++)
	{
		if(aindex[i]!=-1)
		printf(" %d",aindex[i]);

	}
	printf("\n");
	printf("Position where number to be added  :");
	scanf("%d",&pos);
	for(int i=0;i<10;i++)
	{
		if(i==pos)
			arr[i]=number;

	}
	for(int i=0;i<10;i++)
	{
		if(i==pos)
			aindex[i]=-1;

	}
	for(int i=0;i<10;i++)
	{
		if(nindex[i]==-1)
		{
			nindex[i]=pos;
			break;
		}

	}

}
void display(int*arr,int *aindex,int *nindex)
{	int i;
	printf("arr     : ");
	for(i=0;i<10;i++)
	{
		printf(" %d",arr[i]);
	}
	printf("\naindex: ");
	for(i=0;i<10;i++)
	{
		printf(" %d",aindex[i]);
	}
	printf("\nnindex: ");
	for(i=0;i<10;i++)
	{
		printf(" %d",nindex[i]);
	}
	printf("\n");
}
int is_array_empty(int *nindex)
{
	int i,j=0;
		for(i=0;i<10;i++)
			{
				if(nindex[i]==-1)
					j++;
			}
		if(j==10)
			return 1;
		else
			return 0;

}
void delete_number_from_array(int *aindex,int *arr,int*nindex)
{
	int i,pos;
		printf("arr     : \n");

		for(i=0;i<10;i++)
		{	if(nindex[i]!=-1)
			{
				printf("Number: %d, Position: %d\n",arr[nindex[i]],nindex[i]);
			}
		}
	printf("Enter position of number to be deleted :");
	scanf("%d",&pos);
	for(i=0;i<10;i++)
	{
		if(i==pos)
		{
			aindex[i]=i;
			arr[i]=0;
		}
	}
		for(i=0;i<10;i++)
		{
			if(nindex[i]==pos)
				nindex[i]=-1;
		}

	}

int maximum_number_in_array(int *arr)
{
	int max[10],i;
	for(i=0;i<10;i++)
	{
		max[i]=arr[i];
	}
	Select_Sort(max);
	return max[9];

}
void Select_Sort(int *pa)
{
	int i,j,temp;
	for(i=0;i<9;i++)
	{
		for(j=i+1;j<10;j++)
		{
			if(*(pa+i)>*(pa+j))//10,20,30,50,40
			{
				temp=*(pa+i);
				*(pa+i)=*(pa+j);
				*(pa+j)=temp;
			}
		}
	}
}
int minimum_number_in_array(int *arr)
{
	int min[10],i;
	for(i=0;i<10;i++)
	{
		min[i]=arr[i];
	}
		Select_Sort(min);
		return min[0];

}
int sum_of_numbers_in_array(int *arr)
{
	int sum=0;
	for(int i=0;i<10;i++)
	{
		sum=sum+arr[i];
	}

	return sum;
}
