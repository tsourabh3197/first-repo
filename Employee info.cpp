//============================================================================
// Name        : Question_5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string>
using namespace std;
class date
{
	int day;
	int month;
	int year;
public:
	void acceptDate(date &bdate,date &jdate)
	{
		cout<<"Enter day of birth in dd/mm/yy format  :";
		cin>>bdate.day>>bdate.month>>bdate.year;
		cout<<"Enter day of joining in dd/mm/yy format  :";
		cin>>jdate.day>>jdate.month>>jdate.year;
	}
	void printDate(date &bdate,date &jdate)
	{
		cout<<"Birhday         : ";
		cout<<bdate.day<<"/"<<bdate.month<<"/"<<bdate.year<<endl;
		cout<<"Date of joining : ";
		cout<<jdate.day<<"/"<<jdate.month<<"/"<<jdate.year<<endl;
	}
	int claculateYear(date &bdate,date &jdate)
	{
		int Year=jdate.year-bdate.year;
		return Year;
	}
	int calculateExp(date &jdate)
	{
		int month,year,Exp;
		cout<<"Enter current month in number: ";
		cin>>month;
		cout<<"Enter current year in number: ";
		cin>>year;
		//jdate.year+1+ (12-jdate.month)+month;
		if(jdate.year==year)
		{
			return (month-jdate.month);
		}
		else
		{
			Exp=(year-(jdate.year+1))*12+ (12-jdate.month)+month;
			return Exp;
		}

	}
};
class Employee:public date
{
	int empid;
	string name;
	string address;
	float salary;
	date bdate;
	date jdate;
public:
	void acceptRecord()
	{
		cout<<"Enter Empid           :";
		cin>>this->empid;

		cout<<"Enter Name            :";
		cin>>this->name;


		cout<<"Enter Address         :";
		cin>>this->address;
		cout<<"Enter salary          :";
		cin>>this->salary;
		acceptDate(bdate,jdate);

	}
	void printRecord()
	{
		cout<<"Empid   : "<<this->empid<<endl;
		cout<<"Name    : "<<this->name<<endl;
		cout<<"Address : "<<this->address<<endl;
		cout<<"Salary  : "<<this->salary<<endl;
		printDate(bdate,jdate);
	}
	void ageDuringJoinning()
	{
		int age= claculateYear(bdate,jdate);
		cout<<"Age during joining: "<<age<<endl;
	}
	void experience()
	{
		int exp= calculateExp(jdate);
		cout<<"Experience till today: "<<exp<<"Months"<<endl;
	}
};
int main() {
	Employee e1;
	e1.acceptRecord();
	e1.printRecord();
	e1.ageDuringJoinning();
	e1.experience();
	return 0;
}
