/*
 ============================================================================
 Name        : strcpy.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
void mystrcpy(const char *c1,char* c2);
int main(void) {
	setvbuf(stdout,NULL,_IONBF,0);
	char ch[30]="hello";
	char c[30];
	mystrcpy(ch,c);
	return EXIT_SUCCESS;
}
void mystrcpy(const char *c1,char* c2)
{
	int i=0;
		while(c1[i]!='\0')
		{
			c2[i]=c1[i];
			i++;
		}
		c2[i]='\0';
		int k= strlen(c2);
		printf("Copied String..\n");
		for(int j=0;j<=k;j++)
		{
			printf("%c",c2[j]);
		}
		printf("\nOriginal String..\n");
		for(int j=0;j<=k;j++)
			{
				printf("%c",c1[j]);
			}
}

