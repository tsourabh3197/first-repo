#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"library.h"
#include"date.h"
void user_accept(user_t *u)
{
    printf("User details..\n");
    //printf("ID: ");
    //scanf("%d",&u->id);
    u->id=get_next_user_id();
    printf("Name: ");
    scanf("%s",u->name);
    printf("Phone: ");
    scanf("%s",u->phone);
    printf("Email: ");
    scanf("%s",u->email);
    printf("Password: ");
    scanf("%s",u->password);
    strcpy(u->role,ROLE_MEMBER );
}
void user_display(user_t *u)
{
    printf("%d %s %s %s %s %s\n",u->id,u->name,u->phone,u->email,u->password,u->role);
}

void book_accept(book_t *b)
{
    printf("Book details...\n");
    //printf("ID: ");
    //scanf("%d",&b->id);
    b->id=get_next_book_id();
    printf("Name: ");
    scanf("%s",b->name);
    printf("Subject: ");
    scanf("%s",b->subject);
    printf("Author: ");
    scanf("%s",b->author);
    printf("Price: ");
    scanf("%f",&b->price);
    printf("ISBN: ");
    scanf("%s",b->isbn);

}
void book_display(book_t *b)
{
    printf("%d %s %s %s %.2f %s\n",b->id,b->name,b->subject,b->author,b->price,b->isbn);
}

void user_add(user_t *u)
{
    FILE *fp= fopen(USER_DB , "ab");
    if(fp==NULL)
    {
        printf("fail...");
    }
    fwrite(u,sizeof(user_t),1,fp);
    printf("Info. added successfully...\n");
    fclose(fp);
}
int search_user_by_email(user_t *u,char email[])
{
    int found=0;
    FILE *fp=fopen(USER_DB,"rb");
    if(fp==NULL)
    {
        printf("Error...\n");
        return found;
    }
    while(fread(u,sizeof(user_t),1,fp)>0)
    {
        if(strcmp(u->email,email)==0)
        {   
            found=1;
            break;
        }
    }
    fclose(fp);
    return found;
}

int get_next_user_id()
{
    int max=0;
    int size=sizeof(user_t);
    user_t u;
    FILE *fp= fopen(USER_DB,"rb");
    if(fp == NULL)
		return max + 1;
    fseek(fp,-size,SEEK_END);
    if(fread(&u,size,1,fp)>0)
    max=u.id;
    fclose(fp);
    return max+1;
}

int get_next_book_id()
{
    int max=0;
    book_t b;
    size_t size=sizeof(book_t);
    FILE *fp= fopen(BOOK_DB,"rb");
    if(fp == NULL)
		return max + 1;
    fseek(fp,-size,SEEK_END);
    if(fread(&b,size,1,fp)>0)
        max=b.id;
    fclose(fp);
    return max+1;

    
}
void book_find_by_name(char name[])
{
    size_t size= sizeof(book_t);
    book_t b;
    int found=0;
    FILE *fp= fopen(BOOK_DB,"rb");
    if(fp==NULL)
    {
        perror("File can't be opened...\n");
        return;
    }
    
    while(fread(&b,size,1,fp)>0)
    {
        if(strstr(b.name,name)!=NULL)
        {
            book_display(&b);
            found=1;
            
        }
    }
    fclose(fp);
    if(!found)
        printf("No such book is found..\n");

}
void bookcopy_accept(bookcopy_t *b)
{
    printf("Book ID: ");
    scanf("%d",&b->bookid);
    printf("Rack: ");
    scanf("%d",&b->rack);
    strcpy(b->status,STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *c) {
	printf("%d, %d, %d, %s\n", c->id, c->bookid, c->rack, c->status);
}

int get_next_bookcopy_id()
{
    int max=0;
    size_t size= sizeof(bookcopy_t);
    bookcopy_t b;
    FILE *fp= fopen(BOOKCOPY_DB,"rb");
    if(fp==NULL)
    {
        return max+1;

    }
    fseek(fp,-size,SEEK_END);
    if(fread(&b,size,1,fp)>0)
    {
        max=b.id;
    }
    fclose(fp);
    return max+1;
}

void issuerecord_accept(issuerecord_t *rec)                                       
{
    printf("Copy id: ");
    scanf("%d",&rec->copyid);
    printf("Member id: ");
    scanf("%d",&rec->memberid);
    printf("issue ");
	date_accept(&rec->issue_date);
    rec->return_duedate = date_add(rec->issue_date, BOOK_RETURN_DAYS);
    memset(&rec->return_date, 0, sizeof(date_t));
    //date_accept(&rec->issue_date);
	rec->fine_amount = 0.0;
}
void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}

int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

void payment_accept(payment_t *p)
{
    printf("Enter user id: ");
    scanf("%d",&p->userid);
    printf("Enter Amount to be charged: ");
    scanf("%f",&p->amount);
    printf("type (fees/fine): ");
	scanf("%s", p->type);
    
    p->tx_time=date_current();
    if(strcmp(p->type,PAY_TYPE_FEES)==0)
    {
        p->nextpayment_duedate=date_add(p->tx_time,MEMBERSHIP_MONTH_DAYS);
    }
    else
    memset(&p->nextpayment_duedate,0,sizeof(date_t));
}
void payment_display(payment_t *p)
{
    printf("ID: %d userID: %d amount: %f type: %s \n",p->id,p->userid,p->amount,p->type);
    printf("payment ");
	date_print(&p->tx_time);
    printf("\nNext payment due ");
    date_print(&p->nextpayment_duedate);
}

void edit_profile()
{
    user_t nu;
    int user_id,found=0;
    long size= sizeof(user_t);
    printf("Enter user id: ");
    scanf("%d",&user_id);
    FILE *fp= fopen(USER_DB,"rb+");
    if(fp==NULL)
    {
        perror("File can't be opened...\n");
        return;
    }
    while(fread(&nu,size,1,fp)>0)
    {
        if(nu.id==user_id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        char name[30],phone[15],email[20];
        int choice;
        do{
             printf("\n0. out\n1. change name\n2. change phone\n3. change email\nenter choice ");
            scanf("%d",&choice);
            switch(choice)
            {
                case 1:
                    printf("Enter new name: ");
                    scanf("%s",name);
                    strcpy(nu.name,name);
                    break;
                case 2:
                    printf("Enter new phone: ");
                    scanf("%s",phone);
                    strcpy(nu.phone,phone);
                    break;
                case 3:
                    printf("Enter new email: ");
                    scanf("%s",email);
                    strcpy(nu.email,email);
                    printf("Email is updated..\n");
                    break;

            }
        }while(choice!=0);
        fseek(fp,-size,SEEK_CUR);
        user_display(&nu);
        fwrite(&nu,size,1,fp);
       
    }
    else
    {
        printf("No such uder is found..\n");
    }
    
}

void edit_password(){
    user_t nu;
    int user_id,found=0;
    long size= sizeof(user_t);
    printf("Enter user id: ");
    scanf("%d",&user_id);
    FILE *fp= fopen(USER_DB,"rb+");
    if(fp==NULL)
    {
        perror("File can't be opened...\n");
        return;
    }
    while(fread(&nu,size,1,fp)>0)
    {
        if(nu.id==user_id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        char password[15];
        printf("Enter new password: ");
        scanf("%s",password);
       
        fseek(fp,-size,SEEK_CUR);
        strcpy(nu.password,password);
        fwrite(&nu,size,1,fp);
        user_display(&nu);
        
        printf("password is updated..\n");
        
       
    }
    else
    {
        printf("No such uder is found..\n");
    }

}
