/*
 ============================================================================
 Name        : strcmp.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
int mystrcmp(const char *ch,const char *c);
int main(void) {
	char ch[30]="Sourabh";
	char c[30]="SourabhT";
	int cmp=mystrcmp(ch,c);
	if(cmp==0)
		printf("Strings are equal....");

	else
			printf("Strings are not equal %d",cmp);
	return EXIT_SUCCESS;
}
int mystrcmp(const char *ch,const char *c)
{
	int i;
	for(i=0;ch[i]!='\0'&&c[i]!='\0';i++)
		if(ch[i]!=c[i])
			break;
	return(ch[i]-c[i]);
}
