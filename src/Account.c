/*
 * Account.c
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "AccountHolder.h"
#include "Account.h"

void accept_account(account_t *a)
{
	printf("ID: ");
	scanf("%d",&a->id);
	strcpy(a->type,TYPE_SAVING);
	printf("Balance: ");
	scanf("%f",&a->amount);
	accept_Account_Holder(&a->ah);
}

void display_account(account_t *a)
{
	printf("ID: %d\n",a->id);
	printf("Type: %s\n",a->type);
	printf("Balance: %.2f\n",a->amount);
	display_Account_Holder(&a->ah);
}

