/*
 ============================================================================
 Name        : Question_7.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "AccountHolder.h"
#include "Account.h"
#include "linkedlist.h"
int menu_list();
int main(void)
{
	setvbuf(stdout,NULL,_IONBF,0);
	int choice;

	while(1)
	{
		choice= menu_list();
		switch(choice)
		{
		case 0: exit(0);
		case 1:
			add_first();
			break;
		case 2:
			add_last();
			break;
		case 3:
			display_node_forward();
			break;
		case 4:
			display_node_backward();
			break;
		case 5:
			find_by_id();
			break;
		case 6:
			find_by_name();
			break;
		case 7:
			delete_all_functionality();
			break;
		default:
			printf("Enter correct choice!!!\n");

		}
	}

	return EXIT_SUCCESS;
}
int menu_list()
{
	int choice;
	printf("0. EXIT\n");
	printf("1. Add first\n");
	printf("2. Add last\n");
	printf("3. display all forward\n");
	printf("4. display all backward\n");
	printf("5. Find by AC id\n");
	printf("6. Find by user name\n");
	printf("7. Delete all functionality\n");
	printf("Enter your choice: ");
	scanf("%d",&choice);
	return choice;

}
