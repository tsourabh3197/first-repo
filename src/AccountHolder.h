/*
 * AccountHolder.h
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */

#ifndef ACCOUNTHOLDER_H_
#define ACCOUNTHOLDER_H_
typedef struct aHolder{
	char name[30];
	char address[40];
	char contact[20];

}aholder_t;

void accept_Account_Holder(aholder_t *a);
void display_Account_Holder(aholder_t *a);
#endif
