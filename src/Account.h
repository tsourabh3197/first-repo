/*
 * Account.h
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */

#ifndef ACCOUNT_H_
#define ACCOUNT_H_
#include"AccountHolder.h"
#define TYPE_SAVING "saving"
#define TYPE_CURRENT "current"
typedef struct Account{
	int id;
	char type[10];
	float amount;
	aholder_t ah;

}account_t;

void accept_account(account_t *a);
void display_account(account_t *a);

#endif /* ACCOUNT_H_ */
