/*
 * AccountHolder.c
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "AccountHolder.h"
void accept_Account_Holder(aholder_t *a)
{
	scanf("%*c");
	printf("Name: ");
	scanf("%[^\n]s",a->name);
	scanf("%*c");
	printf("Address: ");
	scanf("%[^\n]s",a->address);
	scanf("%*c");
	printf("Contact: ");
	scanf("%[^\n]s",a->contact);
	scanf("%*c");
}

void display_Account_Holder(aholder_t *a)
{
	printf("Name   : %s\n",a->name);
	printf("Address: %s\n",a->address);
	printf("contact: %s\n",a->contact);

}
