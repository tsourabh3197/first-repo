/*
 * linkedlist.h
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
#include "AccountHolder.h"
#include "Account.h"
typedef struct linkedlist{
	account_t a;
	struct linkedlist *next;
	struct linkedlist *prev;
}list_t;

list_t* create_new_node();
void accept_node(list_t *node);
void display_node_forward();
void display_node_backward();
void display_node_backward1(list_t *node);
void add_first();
void add_last();
void find_by_id();
void find_by_name();
void delete_all_functionality();

#endif /* LINKEDLIST_H_ */
