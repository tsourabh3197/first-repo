/*
 * linkedlist.c
 *
 *  Created on: 14-Oct-2020
 *      Author: Sourabh
 */
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "AccountHolder.h"
#include "Account.h"
#include "linkedlist.h"
list_t *head=NULL;
int ct=0;
void add_first()
{
	list_t *nnode=create_new_node();
	if(nnode==NULL)
	{
		printf("Fail to create node\n");
		return;
	}
	if(head==NULL)
	{
		head=nnode;
		printf("First node is added successfully...\n");
	}
	else
	{
		nnode->next=head;
		head->prev=nnode;
		head=nnode;
		nnode=NULL;
		printf("node is added at first place successfully...\n");
	}
}
list_t* create_new_node()
{
	list_t *node= (list_t*)malloc(sizeof(list_t));
	accept_node(node);
	node->next=NULL;
	node->prev=NULL;
	return node;
}
void accept_node(list_t *node)
{
	accept_account(&node->a);
	node->next=NULL;
	node->prev=NULL;
}
void display_node_forward()
{
	list_t *trav=head;
	if(head==NULL)
	{
		printf("List is empty\n");
		return;
	}

	while(trav!=NULL)
	{
		display_account(&trav->a);
		trav=trav->next;
	}
	printf("All nodes are printed in forward direction..\n");

}
void add_last()
{
	if(head==NULL)
	{
		add_first();
		return;
	}

	list_t *nnode=create_new_node();
	if(nnode==NULL)
	{
		printf("Fail to create node\n");
		return;
	}

	else
	{
		list_t *trav=head,*temp;
		while(trav!=NULL)
		{
			temp=trav;
			trav=trav->next;
		}
		temp->next=nnode;
		nnode->prev=temp;
		temp=NULL;
		printf("node is added at last place successfully...\n");
	}
}
void display_node_backward()
{
	 display_node_backward1(head);
}
void display_node_backward1(list_t *node)
{
	if(node==NULL)
	{
		return;
	}
	display_node_backward1(node->next);
	display_account(&node->a);
	printf("All nodes are printed in backward direction..\n");
}
void find_by_id()
{
	list_t *trav=head;
	int id;
	printf("Enter id to be found: ");
	scanf("%d",&id);
	while(trav!=NULL)
	{
		if(trav->a.id==id)
		{
			display_account(&trav->a);
			ct=1;
		}
		trav=trav->next;
	}
	if(!ct)
	{
		printf("no such account is found..\n");
	}
}

void find_by_name()
{
	scanf("%*c");
	char name[30];
	printf("Enter name to search..");

	scanf("%[^\n]s",name);
	list_t *trav=head;

	while(trav!=NULL)
	{
		if(strcmp(trav->a.ah.name,name)==0)
		{
			display_account(&trav->a);
			ct=1;
		}
		trav=trav->next;
	}
	if(!ct)
	{
		printf("no such account is found..\n");
	}
}
void delete_all_functionality()
{
	list_t *trav=head,*temp,*temp1;
	while(trav!=NULL)
	{
		printf("1while\n");
		temp=trav;
		trav=trav->next;
	}
	while((temp->prev)!=NULL)
	{
		printf("2while\n");
		temp1=temp;
		temp->prev->next=NULL;
		temp=temp->prev;
		temp1->prev=NULL;
		free(temp1);
		temp1=NULL;
	}
	free(temp);
	temp=NULL;
	head=NULL;
	printf("All nodes are deleted!!!\n");
}
